# Weather

This application displays the current weather of a given location, in addition to a 7 day forecast. The location is defaulted to New York, however the user has the ability to use their current location or explicitly input a location. 

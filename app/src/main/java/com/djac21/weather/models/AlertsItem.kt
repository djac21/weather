package com.djac21.weather.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class AlertsItem(
    @SerializedName("start")
    val start: Int = 0,
    @SerializedName("description")
    val description: String = "",
    @SerializedName("sender_name")
    val senderName: String = "",
    @SerializedName("end")
    val end: Int = 0,
    @SerializedName("event")
    val event: String = "",
    @SerializedName("tags")
    val tags: List<String>?
) : Serializable
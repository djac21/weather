package com.djac21.weather.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.LinearLayoutManager
import com.djac21.weather.R
import com.djac21.weather.adapters.WeatherAdapter
import com.djac21.weather.models.WeatherModel
import com.djac21.weather.utils.LOCATION_NAME
import com.djac21.weather.utils.WEATHER_MODEL
import kotlinx.android.synthetic.main.activity_details.*

class DetailsActivity : AppCompatActivity() {
    private var viewDataBinding: ViewDataBinding? = null
    private lateinit var weatherModel: WeatherModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewDataBinding = DataBindingUtil.setContentView(this, R.layout.activity_details)

        val bundle = intent.extras
        supportActionBar?.title = bundle?.getString(LOCATION_NAME)
        weatherModel = bundle?.getSerializable(WEATHER_MODEL) as WeatherModel

        viewDataBinding?.setVariable(1, weatherModel)

        val carsAdapter = WeatherAdapter(weatherModel.daily, applicationContext)
        recycler_view.adapter = carsAdapter
        recycler_view.layoutManager = LinearLayoutManager(applicationContext)
    }
}
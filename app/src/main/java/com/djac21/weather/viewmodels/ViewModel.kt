package com.djac21.weather.viewmodels

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.djac21.weather.api.ApiClient
import com.djac21.weather.api.ApiInterface
import com.djac21.weather.models.WeatherModel
import com.djac21.weather.utils.API_KEY
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers

class ViewModel : ViewModel() {
    private var disposable: CompositeDisposable?
    private val liveData: MutableLiveData<WeatherModel> = MutableLiveData<WeatherModel>()
    private val dataError = MutableLiveData<Boolean>()
    private val loading = MutableLiveData<Boolean>()

    fun getLoading(): LiveData<Boolean> {
        return loading
    }

    fun getData(context: Context, lat: String?, lon: String?): LiveData<WeatherModel> {
        fetchRepos(context, lat, lon)

        return liveData
    }

    val error: LiveData<Boolean>
        get() = dataError

    private fun fetchRepos(context: Context, lat: String?, lon: String?) {
        loading.value = true

        ApiClient.getRetrofit(context).create(ApiInterface::class.java).getWeather(lat, lon, API_KEY)
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribeWith(object : DisposableSingleObserver<WeatherModel?>() {
                override fun onSuccess(weatherModel: WeatherModel) {
                    dataError.value = false
                    liveData.value = weatherModel
                    loading.value = false
                }

                override fun onError(e: Throwable) {
                    dataError.value = true
                    loading.value = false
                }
            })?.let {
                disposable?.add(it)
            }
    }

    override fun onCleared() {
        super.onCleared()
        if (disposable != null) {
            disposable!!.clear()
            disposable = null
        }
    }

    init {
        disposable = CompositeDisposable()
    }
}
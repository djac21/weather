package com.djac21.weather.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class WeatherItem(
    @SerializedName("icon")
    val icon: String = "",
    @SerializedName("description")
    val description: String = "",
    @SerializedName("main")
    val main: String = "",
    @SerializedName("id")
    val id: Int = 0
) : Serializable
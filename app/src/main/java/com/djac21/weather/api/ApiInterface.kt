package com.djac21.weather.api

import com.djac21.weather.models.WeatherModel
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiInterface {
    @GET("/data/2.5/onecall?exclude=hourly&units=imperial")
    fun getWeather(@Query("lat") lat: String?, @Query("lon") lon: String?, @Query("appid") apikey: String?): Single<WeatherModel>?
}
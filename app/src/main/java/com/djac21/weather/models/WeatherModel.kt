package com.djac21.weather.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class WeatherModel(
    @SerializedName("alerts")
    val alerts: List<AlertsItem>?,
    @SerializedName("current")
    val current: Current,
    @SerializedName("timezone")
    val timezone: String = "",
    @SerializedName("timezone_offset")
    val timezoneOffset: Int = 0,
    @SerializedName("daily")
    val daily: List<DailyItem>?,
    @SerializedName("lon")
    val lon: Double = 0.0,
    @SerializedName("minutely")
    val minutely: List<MinutelyItem>?,
    @SerializedName("lat")
    val lat: Double = 0.0
) : Serializable
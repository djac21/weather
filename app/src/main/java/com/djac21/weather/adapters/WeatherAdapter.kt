package com.djac21.weather.adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.djac21.weather.R
import com.djac21.weather.databinding.ItemViewBinding
import com.djac21.weather.models.DailyItem
import com.djac21.weather.utils.TODAY
import com.djac21.weather.utils.Utils
import com.djac21.weather.utils.Utils.degreeFormat
import com.djac21.weather.utils.Utils.unixConverter

class WeatherAdapter(private var dailyWeatherList: List<DailyItem>?, private val context: Context) : RecyclerView.Adapter<WeatherAdapter.ViewHolder>() {
    private val TAG = WeatherAdapter::class.java.simpleName

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = ItemViewBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val dailyWeather = dailyWeatherList?.get(position)

        if (position == 0)
            holder.itemViewBinding.date.text = TODAY
        else
            holder.itemViewBinding.date.text = dailyWeather?.let { unixConverter(it.dt) }

        holder.itemViewBinding.high.text = degreeFormat(dailyWeather?.temp?.max)
        holder.itemViewBinding.low.text = degreeFormat(dailyWeather?.temp?.min)

        Glide.with(context)
            .load(Utils.imageURL(dailyWeather?.weather?.get(0)?.icon))
            .apply(
                RequestOptions()
                    .placeholder(R.drawable.ic_no_image)
                    .error(R.drawable.ic_no_image)
            )
            .into(holder.itemViewBinding.image)
    }

    override fun getItemCount(): Int = dailyWeatherList?.size!!

    inner class ViewHolder(var itemViewBinding: ItemViewBinding) : RecyclerView.ViewHolder(itemViewBinding.root), View.OnClickListener {
        override fun onClick(view: View) {
            Log.d(TAG, "onClick: ${dailyWeatherList?.get(absoluteAdapterPosition)}")
        }

        init {
            itemView.setOnClickListener(this)
        }
    }
}

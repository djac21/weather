package com.djac21.weather.activities

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.os.Looper
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.EditorInfo
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.IntentSenderRequest
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.djac21.weather.R
import com.djac21.weather.databinding.ActivityMainBinding
import com.djac21.weather.models.WeatherModel
import com.djac21.weather.utils.GPS_PERMISSION
import com.djac21.weather.utils.LOCATION_NAME
import com.djac21.weather.utils.LOCATION_PERMISSION
import com.djac21.weather.utils.Utils.closeKeyboard
import com.djac21.weather.utils.Utils.imageURL
import com.djac21.weather.utils.Utils.isLocationEnabled
import com.djac21.weather.utils.Utils.isNetworkAvailable
import com.djac21.weather.utils.WEATHER_MODEL
import com.djac21.weather.viewmodels.ViewModel
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.IOException
import java.util.*

class MainActivity : AppCompatActivity() {
    private val TAG = MainActivity::class.java.simpleName
    private lateinit var viewModel: ViewModel
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var currentWeatherModel: WeatherModel
    private lateinit var activityMainBinding: ActivityMainBinding
    private lateinit var resultLauncher: ActivityResultLauncher<IntentSenderRequest>
    private var latitude = 40.72
    private var longitude = -73.99
    private var locationText = "New York"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        viewModel = ViewModelProvider(this).get(ViewModel::class.java)
        getWeather()

        activityMainBinding.locationEditText.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_GO)
                initializeLocationSearch()
            false
        }

        activityMainBinding.searchLocationButton.setOnClickListener {
            initializeLocationSearch()
            closeKeyboard(this)
        }

        activityMainBinding.swipeRefreshLayout.setOnRefreshListener {
            getWeather()
        }

        activityMainBinding.sevenDayForecast.setOnClickListener {
            val intent = Intent(this, DetailsActivity::class.java)
            val bundle = Bundle()
            bundle.putSerializable(WEATHER_MODEL, currentWeatherModel)
            intent.putExtras(bundle)
            intent.putExtra(LOCATION_NAME, locationText)
            startActivity(intent)
        }

        activityMainBinding.fabLocation.setOnClickListener {
            getLastLocation()
        }

        resultLauncher = this.registerForActivityResult(ActivityResultContracts.StartIntentSenderForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK)
                getLastLocation()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == LOCATION_PERMISSION) {
            if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED))
                getLastLocation()
            else
                Snackbar.make(activityMainBinding.swipeRefreshLayout, "Permission denied - Weather by current location is unavailable", Snackbar.LENGTH_LONG).show()
        }
    }

    private fun getWeather() {
        if (!isNetworkAvailable(this))
            Snackbar.make(activityMainBinding.swipeRefreshLayout, "Offline mode - No internet available", Snackbar.LENGTH_SHORT).show()

        viewModel.getLoading().observe(this) { loading: Boolean? ->
            if (loading != null) {
                if (loading) {
                    activityMainBinding.progressBar.visibility = View.VISIBLE
                    activityMainBinding.errorMessage.visibility = View.GONE
                    activityMainBinding.cardView.visibility = View.GONE
                } else {
                    activityMainBinding.progressBar.visibility = View.GONE
                }
            }
        }

        viewModel.getData(this, latitude.toString(), longitude.toString()).observe(this, { weatherModel: WeatherModel? ->
            if (weatherModel != null) {
                activityMainBinding.cardView.visibility = View.VISIBLE

                currentWeatherModel = weatherModel
                activityMainBinding.weather = currentWeatherModel
                activityMainBinding.location.text = locationText

                Glide.with(this)
                    .load(imageURL(weatherModel.current.weather?.get(0)?.icon))
                    .apply(
                        RequestOptions()
                            .placeholder(R.drawable.ic_no_image)
                            .error(R.drawable.ic_no_image)
                    )
                    .into(activityMainBinding.image)
            }

            activityMainBinding.swipeRefreshLayout.isRefreshing = false
        })

        viewModel.error.observe(this, { error: Boolean? ->
            if (error != null)
                if (error) {
                    activityMainBinding.errorMessage.visibility = View.VISIBLE
                    activityMainBinding.cardView.visibility = View.GONE
                } else {
                    activityMainBinding.errorMessage.visibility = View.GONE
                    activityMainBinding.cardView.visibility = View.VISIBLE
                }
            activityMainBinding.swipeRefreshLayout.isRefreshing = false
        })
    }

    private fun initializeLocationSearch() {
        if (!isNetworkAvailable(this))
            Snackbar.make(activityMainBinding.swipeRefreshLayout, "Offline mode - No internet available", Snackbar.LENGTH_SHORT).show()
        else
            getLatLongOrLocation(this, activityMainBinding.locationEditText.text.toString(), 0.0, 0.0, true)
    }

    private fun getLatLongOrLocation(context: Context, locationAddress: String, lat: Double, lon: Double, getLatLong: Boolean) {
        GlobalScope.launch(Dispatchers.IO) {
            val geoCoder = Geocoder(context, Locale.US)
            var updateWeather = false

            try {
                withContext(Dispatchers.Main) {
                    activityMainBinding.swipeRefreshLayout.isRefreshing = true
                }

                val addressList: MutableList<Address>? = if (getLatLong)
                    geoCoder.getFromLocationName(locationAddress, 1)
                else
                    geoCoder.getFromLocation(lat, lon, 1)

                if (addressList != null && addressList.size > 0) {
                    val address = addressList[0]
                    longitude = address.longitude
                    latitude = address.latitude

                    updateWeather = true

                    withContext(Dispatchers.Main) {
                        activityMainBinding.locationEditText.text.clear()
                        locationText = address.locality
                        activityMainBinding.swipeRefreshLayout.isRefreshing = false
                    }

                    Log.d(TAG, "Lat: $latitude \nLon: $longitude \nLoc: ${address.locality}")
                    stopLocationUpdates()
                } else {
                    Snackbar.make(activityMainBinding.swipeRefreshLayout, "No location found", Snackbar.LENGTH_SHORT).show()
                    updateWeather = false

                    withContext(Dispatchers.Main) {
                        activityMainBinding.swipeRefreshLayout.isRefreshing = false
                    }
                }
            } catch (e: IOException) {
                Log.e(TAG, "Unable to connect", e)

                updateWeather = false
                withContext(Dispatchers.Main) {
                    activityMainBinding.swipeRefreshLayout.isRefreshing = false
                }
            } finally {
                if (updateWeather) {
                    withContext(Dispatchers.Main) {
                        getWeather()
                    }
                }
            }
        }
    }

    private fun getLastLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
        ) {
            if (isLocationEnabled(this)) {
                fusedLocationClient.lastLocation.addOnCompleteListener(this) { task ->
                    val location: Location? = task.result
                    if (location != null) {
                        latitude = location.latitude
                        longitude = location.longitude
                        getLatLongOrLocation(this, "", latitude, longitude, false)
                    } else {
                        fusedLocationClient.requestLocationUpdates(locationRequest(), locationCallback, Looper.getMainLooper())
                    }
                }
            } else {
                val builder = LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest())
                    .setAlwaysShow(true)

                val pendingResult = LocationServices
                    .getSettingsClient(this)
                    .checkLocationSettings(builder.build())

                pendingResult.addOnCompleteListener { task ->
                    if (task.isSuccessful.not()) {
                        task.exception?.let {
                            if (it is ApiException && it.statusCode == LocationSettingsStatusCodes.RESOLUTION_REQUIRED) {
                                (it as ResolvableApiException).startResolutionForResult(this, GPS_PERMISSION)
                                val intentSenderRequest = IntentSenderRequest.Builder(it.resolution).build()
                                resultLauncher.launch(intentSenderRequest)
                                stopLocationUpdates()
                            }
                        }
                    }
                }
            }
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q)
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_BACKGROUND_LOCATION), LOCATION_PERMISSION)
            else
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION), LOCATION_PERMISSION)
        }
    }

    private fun locationRequest(): LocationRequest {
        return LocationRequest.create().apply {
            interval = 0
            fastestInterval = 0
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            maxWaitTime = 1000
        }
    }

    private fun stopLocationUpdates() {
        fusedLocationClient.removeLocationUpdates(locationCallback)
    }

    private val locationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            val location: Location = locationResult.lastLocation
            latitude = location.latitude
            longitude = location.longitude
            getLatLongOrLocation(applicationContext, "", latitude, longitude, false)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.refresh_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.refresh)
            getWeather()
        return super.onOptionsItemSelected(item)
    }
}
package com.djac21.weather.utils

const val BASE_URL = "https://api.openweathermap.org"
const val API_KEY = "e8a426cd2ac46976e4d4e9c6e90a51ac"
const val LOCATION_PERMISSION = 1
const val GPS_PERMISSION = 2
const val WEATHER_MODEL = "weather_model"
const val LOCATION_NAME = "location_name"
const val TODAY = "Today"
package com.djac21.weather.utils

import android.app.Activity
import android.content.Context
import android.location.LocationManager
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.view.inputmethod.InputMethodManager
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.roundToInt

object Utils {
    fun isNetworkAvailable(context: Context): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val network = connectivityManager.activeNetwork ?: return false
            val networkCapabilities = connectivityManager.getNetworkCapabilities(network) ?: return false
            return when {
                networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
                networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_BLUETOOTH) -> true
                else -> false
            }
        } else {
            return connectivityManager.activeNetworkInfo?.isConnected ?: false
        }
    }

    fun isLocationEnabled(context: Context): Boolean {
        val locationManager: LocationManager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
    }

    fun imageURL(iconCode: String?): String {
        return "https://openweathermap.org/img/wn/${iconCode}@2x.png"
    }

    fun windDegConverter(windDeg: Int): String {
        if (windDeg <= 0)
            return ""

        val directions = listOf("N", "NE", "E", "SE", "S", "SW", "W", "NW")
        return directions[(((windDeg % 360) / 45).toDouble()).roundToInt() % 8]
    }

    fun unixConverter(unixInt: Int): String {
        val unix = unixInt * 1000L
        val dateFormat = SimpleDateFormat("E", Locale.US)
        val dt = Date(unix)
        return dateFormat.format(dt)
    }

    fun degreeFormat(number: Double?): String {
        return "%.2f \u2109".format(number)
    }

    fun speedFormat(speed: Double?): String {
        if (speed != null && speed <= 0.0)
            return "No Wind"

        return "Wind: %.2f MPH ".format(speed)
    }

    fun closeKeyboard(activity: Activity) {
        val inputManager: InputMethodManager = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputManager.hideSoftInputFromWindow(activity.currentFocus?.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
    }
}
package com.djac21.weather.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class MinutelyItem(
    @SerializedName("dt")
    val dt: Int = 0,
    @SerializedName("precipitation")
    val precipitation: Int = 0
) : Serializable